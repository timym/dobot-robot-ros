#!/usr/bin/env python

import numpy
import rospy
import time
from dobot_magician.srv import *
from dobot_magician.msg import *
from time import sleep



class Dobot_Subscriber_and_Client: # functions used to control dobot

    def init_(self): # initialise all the cartesian and joint angle global variables

        self.current_x = None
        self.current_y = None
        self.current_z = None
        self.current_j1 = None
        self.current_j2 =None
        self.current_j3 = None
        self.current_j4 = None

    def callback(self,the_msg): # allocate new cartesian and joint angles to the global variables
        self.current_x = the_msg.pos.x
        self.current_y = the_msg.pos.y
        self.current_z = the_msg.pos.z
        self.current_j1 = the_msg.joint_angles[0]
        self.current_j2 = the_msg.joint_angles[1]
        self.current_j3 = the_msg.joint_angles[2]
        self.current_j4 = the_msg.joint_angles[3]

    def update_callback(self):

        rospy.Subscriber('/dobot_magician/state',State,self.callback)


    def send_cartesian_position(self,x,y,z):
        rospy.wait_for_service('/dobot_magician/cart_pos')
        try:
            cartesian_service = rospy.ServiceProxy('/dobot_magician/cart_pos', SetPosCart) # subscribe to /dobot_magician/joint_angs service

            cart=Point(x,y,z)
            msg = cartesian_service(cart) # send joint angles
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def send_joint_angles(self,j1, j2, j3, j4):
        rospy.wait_for_service('/dobot_magician/joint_angs')
        try:
            joint_angle_service = rospy.ServiceProxy('/dobot_magician/joint_angs', SetPosAng) # subscribe to /dobot_magician/joint_angs service
            JointAngles = [j1, j2, j3, j4] # set joint angles in rad
            msg = joint_angle_service(JointAngles) # send joint angles


        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
    def send_stepper(self,stepper_select,pps):# 0-stepper 1 & 1-stepper 2, -20000 to 20000 (pulses per second)
        rospy.wait_for_service('/dobot_magician/stepper')
        try:
            stepper_service = rospy.ServiceProxy('/dobot_magician/stepper', SetStepper) # subscribe to /dobot_magician/stepper service

            msg = stepper_service(stepper_select,pps) # send stepper


        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def send_conveyor_belt(self,stepper_select,speed,distance):# 0-stepper 1 & 1-stepper 2, -20000 to 20000 , distance (mm)
            rospy.wait_for_service('/dobot_magician/conveyor')
            try:
                stepper_service = rospy.ServiceProxy('/dobot_magician/conveyor', SetConveyor) # subscribe to /dobot_magician/stepper service
                msg = stepper_service(stepper_select,speed,distance) # send stepper


            except rospy.ServiceException, e:
                print "Service call failed: %s"%e

    def send_linear_rail(self,stepper_select,speed,distance):# 0-stepper 1 & 1-stepper 2, -4000 to 4000, distance (mm)
            rospy.wait_for_service('/dobot_magician/linear_rail')
            try:
                stepper_service = rospy.ServiceProxy('/dobot_magician/linear_rail', SetLinearRail) # subscribe to /dobot_magician/stepper service
                msg = stepper_service(stepper_select,speed,distance) # send stepper


            except rospy.ServiceException, e:
                print "Service call failed: %s"%e

    def send_air(self,ctrlEnable,Gripped): #1-0n & 0-off, 0-ungripped,1-gripped
        rospy.wait_for_service('/dobot_magician/air')
        try:
            pump_service = rospy.ServiceProxy('/dobot_magician/air', SetAir) # subscribe to /dobot_magician/pump service

            msg = pump_service(ctrlEnable,Gripped) # send pump


        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

if __name__ == "__main__":
     rospy.init_node("dobotsample")
     DSC_=Dobot_Subscriber_and_Client() #create object
     DSC_.init_() #initialise
     DSC_.update_callback()


     try:
         while not rospy.core.is_shutdown():
          # write code here
          # examples
          # DSC_.send_pump(0,0) # turn off pump
          #
          # DSC_.send_stepper(0,-5000) # move stepper at 5000 steps per second clockwise

          # DSC_.send_stepper(0,0) # stop stepper motor

          # DSC_.send_linear_rail(0,-4000,150) # plug linear rail in stepper 1 port, move linear rail at 4000 steps per second left at a distance of 150 mm
          # DSC_.send_conveyor_belt(0,-4000,150) # plug conveyor belt in stepper 1 port, move conveyor belt at 4000 steps per second left at a distance of 150 mm

          # DSC_.send_air(1,0) # turn on compressor
          DSC_.send_air(0,1) # turn on suction
          # DSC_.send_cartesian_position(0.2,0.1,-0.02) # get into cartesian position
          # DSC_.send_joint_angles(-0.2, 0.8, 0.6, -0.4) # get into the joint angle positions

          # get cartesian positions
          # print DSC_.current_x
          # print DSC_.current_y
          # print DSC_.current_z

          # get joint angle positions

          # print DSC_.current_j1
          # print DSC_.current_j2
          # print DSC_.current_j3
          # print DSC_.current_j4





     except KeyboardInterrupt:

         print "keyboard interrupt, shutting down"

     rospy.core.signal_shutdown('keyboard interrupt')
