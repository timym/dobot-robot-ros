#!/usr/bin/env python
#
# Author: James Poon, Gavin Paul
# Contributors: Matthew Clout, Sheila Sutjipto, Tim Yan Muk, Dinh Tung Le, Dac Dang Khoa Nguyen


# This node
# - controls the joint angles and cartesian positions of the Dobot Magician
# - get joint states, cartesian positions

# - turns on/off the pump and controls whether to provide suction or compressed air
# - control a stepper motor, conveyor belt and linear Rail



import rospy
from dobot_magician.srv import *
from dobot_magician.msg import *

import serial, struct
import numpy as np
from time import sleep
from threading import Thread, Lock

class Dobot_Magician_node:
  def __init__(self): # 'main'
    rospy.init_node('dobot_magician_node', anonymous=True)
    self._pause_dur = 0.05; self._isEq_timeout = 5
    self._isEq_cerr_m = 0.002; self._isEq_jerr_deg = 1.0
    self._state_pub_dur = 0.1; self._iseq = False;
    # serial
    port_ = rospy.get_param('~port'); baud_ = rospy.get_param('~baud')
    try:
      self._ser = serial.Serial(port_,baud_, timeout=1);
      sleep(1.0); serial_ok_ = True
    except:
      serial_ok_ = False
      print '  Failed to connect to Magician at %s. Exiting ...' % port_

    if serial_ok_ and self._ser.isOpen():
      # declare internal State msg
      self._state_msg = State(); self._state_mtx = Lock()
      self._state_msg.header.frame_id = '/dobot_magician'
      # Home the robot, deactivate pump
      print '  Deactivating pump and calibrating arm to home position ...'
      self._home_pos = [ rospy.get_param('~hX'), rospy.get_param('~hY'), rospy.get_param('~hZ') ]
      self.goHome(self._home_pos[0], self._home_pos[1], self._home_pos[2])
      # raw_input('  Press Enter when calibration complete (beep + green light) to continue ...')
      print '  This node will now pause for 30 seconds ...'; sleep(30.0)


      self._state_pub = rospy.Publisher('/dobot_magician/state', State, queue_size=1)
      suthread_ = Thread(target=self.update_state_loop)
      self._sutrun = True; suthread_.daemon = True; suthread_.start()
      # ROS services
      self._home_srv = rospy.Service('/dobot_magician/home', SetPosCart, self.home_svc)
      self._raw_srv = rospy.Service('/dobot_magician/raw', RawCmd, self.raw_svc)
      self._cart_srv = rospy.Service('/dobot_magician/cart_pos', SetPosCart, self.cart_svc)
      self._jang_srv = rospy.Service('/dobot_magician/joint_angs', SetPosAng, self.jang_svc)
      self._air_srv = rospy.Service('/dobot_magician/air',SetAir, self.air_svc)
      self._stepper_srv = rospy.Service('/dobot_magician/stepper', SetStepper, self.stepper_svc)
      self._conveyor_srv = rospy.Service('/dobot_magician/conveyor', SetConveyor, self.conveyor_svc)
      self._linear_rail_srv = rospy.Service('/dobot_magician/linear_rail', SetLinearRail, self.linear_rail_svc)

      # blocking loop
      print '  Dobot Magician ROS node start!'
      rospy.spin(); self._ser.close()
      self._sutrun = False; suthread_.join(); self._ser.close()

  #dobot serial functions
  def getResponse(self): # get latest response starting from ID (hex)
    nbw_ = self._ser.inWaiting(); buf_ = self._ser.read(nbw_); i_ = nbw_-2
    while i_ > 1:
      if buf_[i_-2]=='\xAA' and buf_[i_-1]=='\xAA': return buf_[i_+1:]
      else: i_ = i_ - 1
    return ""

  def sendCommand(self, length,ID,rw,isQ,params_str=''): # send serial command
    ctrl_ = (isQ<<4) | rw
    chksum_ = 256 - (ID + ctrl_ + sum(bytearray(params_str)))
    while chksum_ < 0: chksum_ = chksum_ + 256
    cmd_str_ = '\xAA\xAA' + chr(length) \
      + chr(ID) + chr(ctrl_) + params_str + chr(chksum_)
    self._ser.flushOutput(); self._ser.write(cmd_str_)
    sleep(self._pause_dur)

  def floats2hstr(self, f): # pack floats to hex string
    f_ = map(float, f)
    return struct.pack('%sf'%len(f_), *f_)

  def separate_32_bit(self,val): #used to separate 32 bit integer to hex sections
     byte1 = (val >> 24) & 0xff;  #bit shift and mask
     byte2 = (val >> 16) & 0xff;
     byte3 = (val >> 8)  & 0xff;
     byte4 = (val & 0xff);
     return [byte1,byte2,byte3,byte4]


  # get joint angle and cartesian positions
  def getState(self):
    # joint angles, end-effector position
    self.sendCommand(2,10,0,1); buf_ = self.getResponse()
    if (len(buf_) > 0) and (ord(buf_[0]) == 10):
      hex_ = buf_[2:34]; pose_arr_ = struct.unpack('%sf'%(len(hex_)/4), hex_)
      self._state_mtx.acquire()
      self._state_msg.header.stamp = rospy.get_rostime()
      self._state_msg.joint_angles = np.deg2rad(pose_arr_[4:]).tolist()
      self._state_msg.pos.x = pose_arr_[0] / 1000
      self._state_msg.pos.y = pose_arr_[1] / 1000
      self._state_msg.pos.z = pose_arr_[2] / 1000
      self._state_msg.pump=self.pump_state
      self._state_mtx.release()
      pos_ok_ = True
    else: pos_ok_ = False

  # control dobot features

  def SetAir(self, isCtrlEnable,isGripped,isQ=0): # pump control
    self.sendCommand(4,63,1,isQ, chr(isCtrlEnable)+chr(isGripped))
    self.pump_state=isCtrlEnable


  def setStepper(self,stepper_select,speed,id=135,rw=0,insEn=1):# 0/1 stepper sel, velocity pulses/sec (+ values clockwise, - values counterclockwise)
       vel=self.separate_32_bit(speed)

       header_and_length = '\xAA\xAA\x08'
       payload=chr(id) + chr(rw) + chr(stepper_select) + chr(insEn) + \
       chr(vel[3]) + chr(vel[2]) + chr(vel[1]) + chr(vel[0])

       twos_compliment = (256)-(id + rw  + stepper_select + insEn + sum(vel))
       chksum_=(twos_compliment  + (1 << 8)) % (1 << 8) # select last 8 bits

       cmd_str_=header_and_length + payload + chr(chksum_)
       self._ser.flushOutput(); self._ser.write(cmd_str_)

  def moveConveyor(self,stepper_select,speed,distance, distance_to_pulses=156.0): #stepper select 0-1, speed pulses per second (-20000 to 20000) , distance mm
     if speed >= -20000 and speed <= 20000 : # limit conveyor speed
          number_of_pulses=distance*distance_to_pulses #convert the wanted distance to pulses
          time_to_complete_distance=number_of_pulses/abs(speed) #find out how much time is required to get to wanted distance

          self.setStepper(stepper_select,speed) #begin moving the conveyor belt
          sleep(time_to_complete_distance) # wait for time to lapse
          self.setStepper(stepper_select,0) #stop conveyor belt
     else:
          str = "minimum speed for conveyor belt is -20000 pulses/second, maximum speed for conveyor belt is 20000 pulses/second, please select a speed between -20000 and 20000"
          rospy.logerr(str)

  def moveLinearRail(self,stepper_select,speed,distance, distance_to_pulses=84.0): #stepper select 0-1, speed pulses per second (-4000 to 4000) , distance mm

      if speed >= -4000 and speed <= 4000 : # limit linear rail speed
          number_of_pulses=distance*distance_to_pulses #convert the wanted distance to pulses
          time_to_complete_distance=number_of_pulses/abs(speed) #find out how much time is required to get to wanted distance
          self.setStepper(stepper_select,speed) #begin moving the linear rail
          sleep(time_to_complete_distance) # wait for time to lapse
          self.setStepper(stepper_select,0) #stop linear rail
      else:
          str = "minimum speed for linear rail is -4000 pulses/second, maximum speed for linear rail is 4000 pulses/second, please select a speed between -4000 and 4000"
          rospy.logerr(str)

  def setCart(self, x,y,z,mode=2,isQ=1): # Cartesian control
      cartlist_ = [ x*1000, y*1000, z*1000,0]
      cmd_str_ = chr(mode) + self.floats2hstr(cartlist_)
      self.sendCommand(19,84,1,isQ,cmd_str_); self.isEq_cart(x,y,z)

  def setJangs(self, angles,mode=4,isQ=0): # Joint angle control
    degslist_ = np.rad2deg(angles).tolist()
    cmd_str_ = chr(mode) + self.floats2hstr(degslist_)
    self.sendCommand(19,84,1,isQ,cmd_str_); self.isEq_jangs(angles)

  #SET the PTP velocity and acceleration for xyz and r
  def setVelPTP(self, xyz_vel_ptp, r_vel_ptp, xyz_accel_ptp, r_accel_ptp, isQ=0):
    vel_ptp_list_ = [ xyz_vel_ptp, r_vel_ptp, xyz_accel_ptp, r_accel_ptp ]  # The default values are [200,100,200,100]
    cmd_str_ = self.floats2hstr(vel_ptp_list_)
    self.sendCommand(18,81,1,isQ,cmd_str_)

  def getVelPTP(self):  #GET the PTP velocity and acceleration of xyz and r
    self.sendCommand(2,81,0,0); buf_ = self.getResponse()
    if (len(buf_) > 0) and (ord(buf_[0]) == 81):
      hex_ = buf_[2:18]; vel_ptp_arr_ = struct.unpack('%sf'%(len(hex_)/4), hex_)
      self._vel_ptp_msg.header.stamp = rospy.get_rostime()
      self._vel_ptp_msg.xyz_vel_ptp = vel_ptp_arr_[0]
      self._vel_ptp_msg.r_vel_ptp = vel_ptp_arr_[1]
      self._vel_ptp_msg.xyz_accel_ptp = vel_ptp_arr_[2]
      self._vel_ptp_msg.r_accel_ptp = vel_ptp_arr_[3]
      vel_ptp_ok_ = True
    else: vel_ptp_ok_ = False


  def goHome(self, x,y,z): # deactivate pump, go to Home position
    self.SetAir(0,0) #turn off pump
    self.pump_state=0 #set pump state to 0
    sleep(self._pause_dur) #wait
    self.setJangs([0,0.4,0.3,0]) # 45 deg j2 and j3
    sleep(self._pause_dur) #wait
    home_str_ = self.floats2hstr([x*1000,y*1000,z*1000,0]) #set home positions
    self.sendCommand(18,30,1,1, home_str_)
    sleep(self._pause_dur) #wait
    self.sendCommand(3,31,1,1, chr(0))


  # set and get inputs and outputs
  def setIODO(self, address, insEn):    #Set and I/O Digital Output (HIGH/LOW)
      #address: 1~20
      self.sendCommand(4,130,1,1, chr(address) + chr(1)) #Set pin at address to IO OUTPUT
      self.sendCommand(4,131,1,1, chr(address) + chr(insEn))

  def getIODI(self, address):# todo further testing required
      self.sendCommand(4,130,1,1, chr(address) + chr(6)) #Set pin at address to IO PULLDOWN INPUT
      self.sendCommand(3,133,0,0, chr(address))
      sleep(1)
      buf_ = self.getResponse()
      # print(len(buf_))
      # print(ord(buf_[0]))
      # print(ord(buf_[1]))
      # print(ord(buf_[2]))
      # print(ord(buf_[3]))
      # print(ord(buf_[4]))

  #Todo setter and getter for input

  #checks and corrections
  def isEq_cart(self, x,y,z):# check catesian and adjust
      t_ = 0; t0_ = rospy.get_rostime().to_sec();
      self._iseq = True; isEq_ = False
      while t_<self._isEq_timeout and not isEq_:
        self.getState()
        self._state_mtx.acquire()
        diff_ = [ self._state_msg.pos.x-x, \
          self._state_msg.pos.y-y, \
          self._state_msg.pos.z-z ]
        self._state_mtx.release()
        isEq_ = np.amax(np.fabs(diff_)) <= self._isEq_cerr_m
        t_ = rospy.get_rostime().to_sec() - t0_
      self._iseq = False

  def isEq_jangs(self, rads):# check joint angles and adjust
      t_ = 0; t0_ = rospy.get_rostime().to_sec();
      self._iseq = True; isEq_ = False
      while t_<self._isEq_timeout and not isEq_:
        self.getState()
        self._state_mtx.acquire()
        diff_ = np.asarray(self._state_msg.joint_angles) - np.asarray(rads)
        self._state_mtx.release()
        isEq_ = np.rad2deg(np.amax(np.fabs(diff_))) <= self._isEq_jerr_deg
        t_ = rospy.get_rostime().to_sec() - t0_
      self._iseq = False

  # ROS related
  def update_state_loop(self): # publish state thread
    self.getState()
    while self._sutrun:
      if not self._iseq: self.getState()
      self._state_mtx.acquire()
      self._state_pub.publish(self._state_msg)
      self._state_mtx.release()
      sleep(self._state_pub_dur)

  def home_svc(self, req): # go Home
    if req.pos.x==0.0 and req.pos.y==0.0 and req.pos.z==0.0:
      self.goHome(self._home_pos[0], self._home_pos[1], self._home_pos[2])
    else: self.goHome(req.pos.x, req.pos.y, req.pos.z)
    return []

  def raw_svc(self, req): # raw serial commands
    self.sendCommand(req.len,req.ID,req.rw,req.isQ,req.cmd)
    return RawCmdResponse(res=self.getResponse())

  def cart_svc(self, req): # set Cartesian end effector position
    self.setCart(req.pos.x, req.pos.y, req.pos.z); return []

  def jang_svc(self, req): # set joint angles
    self.setJangs(req.joint_angles); return []

  def air_svc(self, req): # set pump
    self.SetAir(req.ctrlEnable,req.Gripped); return []

  def stepper_svc(self, req): # set stepper
    self.setStepper(req.stepper_sel,req.speed); return []

  def conveyor_svc(self, req): # move conveyor
    self.moveConveyor(req.stepper_sel,req.speed,req.distance); return []

  def linear_rail_svc(self, req): # move Linear rail
    self.moveLinearRail(req.stepper_sel,req.speed,req.distance); return []



if __name__ == '__main__':
  dm_node_ = Dobot_Magician_node()
